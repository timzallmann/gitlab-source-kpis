const axios = require('axios');
const { combineFrontendCoverage } = require('./combineReports.js');

async function checkFrontendJob(job) {
  // https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/245595221/raw

  const frontendCoverage = {
    overallCoverage: null,
    overallCoverageAbsolute: null,
    overallCoverageTotal: null,
  };
  const rawLogFileRequest = await axios.get(job.web_url + '/raw');
  if (rawLogFileRequest.status === 200) {
    const log = rawLogFileRequest.data;

    // Doing some splitting magic on the logfile
    const startIndex = log.indexOf(
      '=============================== Coverage summary ==============================='
    );
    const coverageString = log.substr(
      startIndex,
      log.indexOf(
        '================================================================================'
      ) - startIndex
    );
    const covLines = coverageString.split('\n');
    for (line of covLines) {
      line = line.replace(')', '');
      const cells = line.split(':');
      if (cells.length > 1) {
        const coverageType = cells[0].trim();
        if (coverageType === 'Statements') {
          const covValues = cells[1].split('(');

          const percValue = Number(covValues[0].replace('%', ''));
          const absValues = covValues[1].split('/');
          const reachedValue = Number(absValues[0]);
          const totalValue = Number(absValues[1]);

          frontendCoverage.overallCoverage = percValue;
          frontendCoverage.overallCoverageAbsolute = reachedValue;
          frontendCoverage.overallCoverageTotal = totalValue;
        }
      }
    }
  } else if (job.coverage && !isNaN(job.coverage)) {
    frontendCoverage.overallCoverage = job.coverage;
  }

  return frontendCoverage;
}

async function checkBackendJob(job) {
  // https://gitlab.com/gitlab-org/gitlab-ce/-/jobs/245595221/raw

  const backendCoverage = {
    backendCoverage: null,
    backendCoverageAbsolute: null,
    backendCoverageTotal: null,
  };
  const rawLogFileRequest = await axios.get(job.web_url + '/raw');
  if (rawLogFileRequest.status === 200) {
    const log = rawLogFileRequest.data;

    const reg = /(\d*) \/ (\d*) LOC \((\d*\.\d{1,2})\%\) covered\./gm;

    let match;
    do {
      match = reg.exec(log);
      if (match) {
        //console.log(m[1], m[2]);
        backendCoverage.backendCoverage = Number(match[3]);
        backendCoverage.backendCoverageAbsolute = Number(match[2]);
        backendCoverage.backendCoverageTotal = Number(match[1]);
      }
    } while (match);
  } else if (job.coverage && !isNaN(job.coverage)) {
    backendCoverage.backendCoverage = job.coverage;
  }

  return backendCoverage;
}

// https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-ce/pipelines/69431148/jobs
//
// Job
// "name": "karma",
// "coverage": 54.22,
// Checks all jobs for a pipeline
exports.checkPipeline = async function(projectId, pipelineId) {
  console.log('Checking the pipeline : ' + pipelineId);

  const responsePage1 = await axios.get(
    `https://gitlab.com/api/v4/projects/${projectId}/pipelines/${pipelineId}/jobs`,
    {
      params: {
        'scope[]': 'success',
        per_page: 100,
      },
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_API_KEY,
      },
    }
  );

  if (responsePage1.data) {
    const responsePage2 = await axios.get(
      `https://gitlab.com/api/v4/projects/${projectId}/pipelines/${pipelineId}/jobs`,
      {
        params: {
          'scope[]': 'success',
          per_page: 100,
          page: 2,
        },
        headers: {
          'PRIVATE-TOKEN': process.env.GITLAB_API_KEY,
        },
      }
    );

    let allJobs = responsePage1.data.concat(responsePage2.data);

    if (responsePage2.data) {
      const responsePage3 = await axios.get(
        `https://gitlab.com/api/v4/projects/${projectId}/pipelines/${pipelineId}/jobs`,
        {
          params: {
            'scope[]': 'success',
            per_page: 100,
            page: 3,
          },
          headers: {
            'PRIVATE-TOKEN': process.env.GITLAB_API_KEY,
          },
        }
      );

      allJobs = allJobs.concat(responsePage3.data);
    }

    console.log('Got the pipelines data for ' + pipelineId);

    let coverageStats = {
      karmaCoverage: null,
      karmaCoverageAbsolute: null,
      karmaCoverageTotal: null,
      jestCoverage: null,
      jestCoverageAbsolute: null,
      jestCoverageTotal: null,
      backendCoverage: null,
      backendCoverageAbsolute: null,
      backendCoverageTotal: null,
    };
    const jestJobs = [];

    for (const job of allJobs) {
      const jobType = job.name.split(' ')[0];
      switch (jobType) {
        case 'karma':
          const karmaResult = await checkFrontendJob(job);
          coverageStats = {
            ...coverageStats,
            karmaCoverage: karmaResult.overallCoverage,
            karmaCoverageAbsolute: karmaResult.overallCoverageAbsolute,
            karmaCoverageTotal: karmaResult.overallCoverageTotal,
          };
          break;
        case 'jest':
          jestJobs.push(job);
          break;
        case 'rspec:coverage':
          const backendCoverage = await checkBackendJob(job);
          coverageStats = { ...coverageStats, ...backendCoverage };
          break;
      }
    }

    if (jestJobs.length) {
      const jestResult = await combineFrontendCoverage(projectId, jestJobs);

      coverageStats = {
        ...coverageStats,
        jestCoverage: jestResult.pct,
        jestCoverageAbsolute: jestResult.covered,
        jestCoverageTotal: jestResult.total,
      };
    }

    return coverageStats;
  } else {
    return null;
  }
};
