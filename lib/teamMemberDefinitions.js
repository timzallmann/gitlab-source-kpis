const fs = require('fs');
const path = require('path');
const Firestore = require('@google-cloud/firestore');

const axios = require('axios');
const YAML = require('yaml');

const PROJECTID = 'gl-source-kpi-collector';
const COLLECTION_NAME = 'glusers';
const firestore = new Firestore({
  projectId: PROJECTID,
});

const fullnamesList = {};

async function getUserIdForUserName(userName) {
  try {
    const userDoc = await firestore
      .collection(COLLECTION_NAME)
      .doc(userName)
      .get();

    if (userDoc && userDoc.exists) {
      return userDoc.data().id;
    } else {
      const response = await axios.get('https://gitlab.com/api/v4/users?username=' + userName);
      if (response.data) {
        if (response.data.length > 0) {
          const userData = {
            id: response.data[0].id,
            name: userName,
          };

          await firestore
            .collection(COLLECTION_NAME)
            .doc(userName)
            .set(userData);

          return response.data[0].id;
        }
      }
    }
  } catch (err) {
    console.error(err);
  }
}

async function findReportsForSlug() {
  // Download Team.yaml
  const teamResponse = await axios.get(
    'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml'
  );
  const team = YAML.parse(teamResponse.data);

  console.log('Downloaded Team.YML');

  // Download fullnames CSV
  const namesResponse = await axios.get(
    'https://docs.google.com/spreadsheets/d/e/2PACX-1vS5LMeEiNl35ZxVTu3lJ8-xuD1kqAby29UOJ0441fowVgZT7UTa-TBnWYm5RzSnIallnWM83QoQqFT-/pub?gid=0&single=true&output=csv'
  );

  console.log('Downloaded Fullnames CSV');

  const nameLines = namesResponse.data.split('\n');
  nameLines.shift();
  nameLines.forEach(line => {
    const nameValues = line.split(',');
    fullnamesList[nameValues[0]] = nameValues[1].replace('\r', '');
  });

  const reports = [];
  team.forEach(member => {
    if (member.departments.indexOf('Development Department') > -1) {
      if (member.type === 'person' && member.gitlab) {
        reports.push(member);
      } else if (member.type === 'person' && !member.gitlab) {
        console.warn(`${member.name} has no GitLab ID in team.yml`);
      }
    }
  });
  return reports;
}

async function getFullReportInfosForManagerSlug() {
  const reports = await findReportsForSlug();

  for (let ind = 0; ind < reports.length; ind++) {
    let selectedReport = reports[ind];
    selectedReport.gitlabId = await getUserIdForUserName(selectedReport.gitlab);

    selectedReport.section = null;
    selectedReport.team = null;

    selectedReport.isManager =
      selectedReport.role.includes('Manager') || selectedReport.role.includes('Director');

    if (selectedReport.role.includes('Director')) {
      selectedReport.team = 'director';
    }

    //Lets figure out the current level of the person
    selectedReport.level = 'intermediate';
    if (selectedReport.role.includes('Senior')) {
      selectedReport.level = 'senior';
    } else if (selectedReport.role.includes('Staff')) {
      selectedReport.level = 'staff';
    } else if (selectedReport.role.includes('Distinguished')) {
      selectedReport.level = 'distinguished';
    } else if (selectedReport.role.includes('Fellow')) {
      selectedReport.level = 'fellow';
    } else if (selectedReport.role.includes('Senior Engineering Manager')) {
      selectedReport.level = 'senior-manager';
    } else if (selectedReport.role.includes('Engineering Manager')) {
      selectedReport.level = 'manager';
    } else if (selectedReport.role.includes('Director')) {
      selectedReport.level = 'director';
    }

    // Lets figure out the actual team
    selectedReport.departments.forEach(department => {
      if (department.includes('Section')) {
        selectedReport.section = department
          .replace('Section', '')
          .trim()
          .toLowerCase();
      } else if (department.includes('Sub-department')) {
        selectedReport.section = department
          .replace('Sub-department', '')
          .trim()
          .toLowerCase();
      } else if (department.includes('Team') && !selectedReport.team) {
        let teamname = department
          .replace('Team', '')
          .trim()
          .toLowerCase()
          .replace(/:/gi, '_')
          .replace(/ /gi, '_');

        if (teamname.includes('_be') || teamname.includes('_fe')) {
        } else {
          if (selectedReport.departments.includes('Frontend')) {
            teamname += '_fe';
          }
        }

        selectedReport.team = teamname;
      }
    });

    selectedReport.technology = '';
    if (selectedReport.team) {
      if (selectedReport.team.includes('_fe')) {
        selectedReport.technology = 'frontend';
      } else if (selectedReport.team.includes('_be')) {
        selectedReport.technology = 'backend';
      } else if (selectedReport.team == 'gitaly') {
        selectedReport.technology = 'go';
      } else if (selectedReport.team == 'gitter') {
        selectedReport.technology = 'node_fullstack';
      } else if (
        selectedReport.team == 'expansion' ||
        selectedReport.team == 'acquisition' ||
        selectedReport.team == 'retention' ||
        selectedReport.team == 'static_site_editor' ||
        selectedReport.team == 'conversion'
      ) {
        selectedReport.technology = 'fullstack';
      }
    } else {
      console.warn(`${selectedReport.name} has no team name`);
    }

    selectedReport.matchName = selectedReport.name;
    if (fullnamesList[selectedReport.gitlabId]) {
      selectedReport.matchName = fullnamesList[selectedReport.gitlabId];
    }

    // Lets set Maintainerships
    const glMemberships =
      selectedReport.projects && selectedReport.projects.gitlab
        ? selectedReport.projects.gitlab
        : [];
    selectedReport.isFrontendTraineeMaintainer = glMemberships.includes(
      'trainee_maintainer frontend'
    );
    selectedReport.isFrontendMaintainer = glMemberships.includes('maintainer frontend');
    selectedReport.isBackendTraineeMaintainer = glMemberships.includes(
      'trainee_maintainer backend'
    );
    selectedReport.isBackendMaintainer = glMemberships.includes('maintainer backend');
    selectedReport.isDatabaseTraineeMaintainer = glMemberships.includes(
      'trainee_maintainer database'
    );
    selectedReport.isDatabaseMaintainer = glMemberships.includes('maintainer database');

    if (!selectedReport.section) {
      console.warn(`${selectedReport.name} has no section name`);
    }
    if (!selectedReport.team && !selectedReport.role.includes('Director')) {
      console.warn(`${selectedReport.name} has no team name`);
    }
  }

  return reports.filter(rep => typeof rep.gitlabId !== 'undefined');
}

exports.getAllReports = async function() {
  let allReports = await getFullReportInfosForManagerSlug();

  allReports.sort((a, b) => (a.team < b.team ? -1 : a.team > b.team ? 1 : 0));

  allReports.forEach(report => {
    delete report.slug;
    delete report.reports_to;
    delete report.projects;
    delete report.type;
    delete report.story;
    delete report.twitter;
    delete report.picture;
    delete report.role;
    delete report.departments;
  });

  return allReports;
};
