const axios = require('axios');

const { checkCommit } = require('./checkCommit');

const checkCommitsInBranch = async function(projectId, pageNr) {
  console.log('===================================================================');
  console.log('Checking Page ' + pageNr + ' for project ' + projectId);
  const commitsRequest = await axios.get(
    `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
    {
      params: {
        per_page: 20,
        page: pageNr,
      },
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_API_KEY,
      },
    }
  );

  if (commitsRequest.status !== 200) {
    console.warn('Page resulted in Error ' + commitsRequest.status);
    return null;
  }

  const commitsData = [];
  let checkedCount = 0;

  for (const commit of commitsRequest.data) {
    if (commit.parent_ids.length > 1) {
      console.log('Check Commit ' + commit.created_at);
      let commitInfo = await checkCommit(projectId, commit.id);
      if (commitInfo) {
        commitsData.push(commitInfo);
        checkedCount += 1;
        console.log(
          'Checked ' +
            checkedCount +
            ' of ' +
            commitsRequest.data.length +
            ' on page ' +
            pageNr +
            ' (' +
            commit.id +
            ')'
        );
      }
    }
  }

  return commitsData;
};

// Checks for a specific branch all commits and their pipelines
exports.checkBranch = async function(projectId, startPage = 1, checkPages = 100) {
  // TODO Paginate through more records
  let lastPage = null;
  for (let currentPage = startPage; currentPage < startPage + checkPages; currentPage++) {
    console.log('Next up page:' + currentPage);
    lastPage = await checkCommitsInBranch(projectId, currentPage);
  }

  return lastPage;
};
