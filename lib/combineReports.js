const axios = require('axios');
const libCoverage = require('istanbul-lib-coverage');

const map = libCoverage.createCoverageMap();
const summary = libCoverage.createCoverageSummary();

const normalizeJestCoverage = obj => {
  const result = { ...obj };

  Object.entries(result).forEach(([k, v]) => {
    if (v.data) result[k] = v.data;
  });

  return result;
};

exports.combineFrontendCoverage = async function(projectId, jobs) {
  const promises = jobs.map(async job => {
    console.log(`Checking coverage for ${job.name}`);
    return await axios.get(
      `https://gitlab.com/api/v4/projects/${projectId}/jobs/${
        job.id
      }/artifacts/coverage-frontend/${job.name
        .replace(' ', '-')
        .replace('/', '-')}/coverage-final.json`,
      {
        headers: {
          'PRIVATE-TOKEN': process.env.GITLAB_API_KEY,
        },
      }
    );
  });

  const coverageReports = await Promise.all(promises);

  coverageReports.forEach(report => {
    const jsonCoverageMap = libCoverage.createCoverageMap(
      normalizeJestCoverage(report.data)
    );
    map.merge(jsonCoverageMap);
  });

  map.files().forEach(function(file) {
    const fileCoverage = map.fileCoverageFor(file);

    summary.merge(fileCoverage.toSummary());
  });

  console.log('Global summary', summary.data.statements);
  return summary.data.statements;
};
