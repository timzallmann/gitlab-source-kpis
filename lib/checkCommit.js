const Firestore = require('@google-cloud/firestore');
const axios = require('axios');
const { checkPipeline } = require('./checkPipeline');

const PROJECTID = 'gl-source-kpi-collector';
const COLLECTION_NAME = 'commits';
const firestore = new Firestore({
  projectId: PROJECTID,
});

exports.checkCommitTest = async function(projectId, commitId) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(true);
    }, Math.random() * 1000);
  });
};

// Check a commit on the specific branch
// https://gitlab.com/api/v4/projects/gitlab-org%2Fgitlab-ce/repository/commits/b7ba5571128a59f12cbef40652717925ff779f4e
exports.checkCommit = async function(projectId, commitId) {
  console.log(`Check Commit : ${commitId} for project ${projectId}`);
  const commitDoc = await firestore
    .collection(COLLECTION_NAME)
    .doc(commitId)
    .get();

  if (commitDoc && commitDoc.exists && commitDoc.data().backendCoverage) {
    return commitDoc.data();
  }

  const commitRequest = await axios.get(
    `https://gitlab.com/api/v4/projects/${projectId}/repository/commits/${commitId}`,
    {
      headers: {
        'PRIVATE-TOKEN': process.env.GITLAB_API_KEY,
      },
    }
  );

  if (commitRequest.status !== 200) {
    console.log(
      'Commit Request no success : ' +
        `https://gitlab.com/api/v4/projects/${projectId}/repository/commits/${commitId}`
    );
    return null;
  }

  const commit = commitRequest.data;
  if (commit.last_pipeline) {
    let pipelineResult;
    if (commit.last_pipeline.status === 'success') {
      console.log('Checking pipelines for ' + commitId + ' => ' + commit.last_pipeline.id);
      pipelineResult = await checkPipeline(projectId, commit.last_pipeline.id);
    } else {
      console.log('Bad pipeline for ' + commitId);
      pipelineResult = {
        karmaCoverage: null,
        karmaCoverageAbsolute: null,
        karmaCoverageTotal: null,
        jestCoverage: null,
        jestCoverageAbsolute: null,
        jestCoverageTotal: null,
        backendCoverage: null,
        backendCoverageAbsolute: null,
        backendCoverageTotal: null,
      };
    }

    console.log('Got data for ' + commitId);

    const commitData = {
      id: commit.id,
      commitDate: new Date(commit.committed_date).getTime(),
      greenPipeline: commit.last_pipeline.status === 'success' || false,
      ...pipelineResult,
    };

    await firestore
      .collection(COLLECTION_NAME)
      .doc(commit.id)
      .set(commitData);

    console.log('Saving for ' + commitId);

    return commitData;
  }

  return null;
};
