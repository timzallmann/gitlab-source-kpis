const functionCode = require('./index.js');
const express = require('express');

const PORT = 8080;

require('dotenv').config();

// Start local HTTP server
const app = express();
const server = require(`http`).createServer(app);
server.on('connection', socket => socket.unref());
server.listen(PORT);

// Register HTTP handlers
Object.keys(functionCode).forEach(gcfFn => {
  // Handle a single HTTP request
  const handler = (req, res) => {
    functionCode[gcfFn](req, res);
  };

  console.log('Register Route : /' + gcfFn);

  app.get(`/${gcfFn}`, handler);
});

console.log('Server running at http://localhost:8080/updateStats');
